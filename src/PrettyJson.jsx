export const PrettyJson = ({data}) => {
  return <div><pre className="code">{JSON.stringify(data, null, 2)}</pre></div>;
}
