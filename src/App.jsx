import { useState } from "react";
import reactLogo from "./assets/react.svg";
import viteLogo from "/vite.svg";
import "./App.css";
import { PrettyJson } from "./PrettyJson";

function App() {
  const [mxid, setMxid] = useState("");
  const [accessToken, setAccessToken] = useState("");
  const [error, setError] = useState("");
  const [ok, setOk] = useState(false);

  const widgetConfig = {
    stickerpicker: {
      content: {
        type: "m.stickerpicker",
        url: "https://sticker.chaos.gratis/?theme=$theme",
        name: "Stickerpicker",
        creatorUserId: mxid,
        data: {},
      },
      sender: mxid,
      state_key: "stickerpicker",
      type: "m.widget",
      id: "stickerpicker",
    },
  };

  const autoSetup = async (e) => {
    e.preventDefault();
    setError("");

    const url = `https://matrix.ccc-ffm.de/_matrix/client/v3/user/${encodeURIComponent(
      mxid
    )}/account_data/m.widgets`;
    try {
      const response = await fetch(url, {
        method: "PUT",
        headers: {
          "Content-Type": "application/json",
          Authorization: `Bearer ${accessToken}`,
        },
        body: JSON.stringify(widgetConfig),
      });

      if (!response.ok) {
        setError("Fehler: " + response.status);
      }

      setOk(true);
    } catch (e) {
      setError("Fehler: " + e.message);
    }
  };

  const wo = (
    <div>
      <b>Wo finde ich den?</b>
      <ul>
        <li>Element Web:</li>
        <ul>
          <li>Einstellungen</li>
          <li>„Hilfe und Info“</li>
          <li>„Zugriffstoken“</li>
        </ul>
        <li>Element Android:</li>
        <ul>
          <li>Einstellungen</li>
          <li>„Erweiterte Einstellungen“</li>
          <li>„Entwicklermodus“ aktivieren</li>
          <li>Ganz unten „Zugriffstoken“</li>
          <li>„Entwicklermodus“ wieder deaktivieren</li>
        </ul>
      </ul>
    </div>
  );

  const section2a = mxid.endsWith("ccc-ffm.de") ? (
    <div className="section">
      <h2>2.a Automatisch einrichten</h2>

      <div className="form-group">
        <label>Access Token</label>
        <input
          type="password"
          value={accessToken}
          onChange={(e) => setAccessToken(e.target.value)}
          autoComplete="off"
        />
        {!accessToken && wo}
        <br />
        {accessToken && (
          <button onClick={autoSetup}>Sticker picker einrichten</button>
        )}
        {
          ok && <div>Einrichtung erfolgreich. Ggf. Clients neu starten.</div>
        }
      </div>
    </div>
  ) : null;

  const section2b = mxid ? (
    <div className="section">
      <h2>2.b Manuell einrichten</h2>

      <ul>
        <li>
          Im Element Web Client in irgendeinem Raum <code>/devtools</code>
        </li>{" "}
        eingeben
        <li>„Kontodaten erkunden“ anklicken</li>
        <li>„Sende benutzerdefiniertes Kontodatenereignis“ anklicken</li>
        <li>Eventtyp „m.widgets“ eingeben</li>
        <li>Den Inhalt mit der folgenden JSON-Wurst ersetzen</li>
        <li>„Senden“ anklicken</li>
        <li>Wenn erfolgreich gesendet, dann sollte der Stickerpicker z.B. unter den … aufrufbar sein</li>
      </ul>

      <PrettyJson data={widgetConfig} />
    </div>
  ) : null;

  return (
    <div className="App">
      <h1>Cyber-Sticker Einrichtung</h1>

      <div className="section">
        <h2>1. Matrix-Id eingeben</h2>

        <div className="form-group">
          <label>Matrix-Id</label>
          <input
            type="text"
            placeholder="@beispiel:ccc-ffm.de"
            value={mxid}
            onChange={(e) => setMxid(e.target.value)}
          />
        </div>
      </div>

      {section2a}
      {section2b}
    </div>
  );
}

export default App;
